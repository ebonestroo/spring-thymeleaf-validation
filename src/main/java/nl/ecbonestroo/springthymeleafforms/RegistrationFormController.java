package nl.ecbonestroo.springthymeleafforms;

import lombok.extern.slf4j.Slf4j;
import nl.ecbonestroo.springthymeleafforms.dto.RegistrationCommand;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Slf4j
@RequestMapping("/registration")
public class RegistrationFormController {

    @ModelAttribute
    public RegistrationCommand registrationRequest() {
        return new RegistrationCommand();
    }

    @GetMapping
    public String registrationForm(){
        return "registrationForm";
    }

    @PostMapping
    public String registerNewUser(@Valid RegistrationCommand registrationCommand, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.registrationCommand", bindingResult);
            redirectAttributes.addFlashAttribute("registrationCommand", registrationCommand);
            return "redirect:/registration";
        }
        log.info(registrationCommand.toString());
        return "redirect:/";
    }
}
