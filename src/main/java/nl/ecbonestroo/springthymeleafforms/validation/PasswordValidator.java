package nl.ecbonestroo.springthymeleafforms.validation;

import nl.ecbonestroo.springthymeleafforms.dto.RegistrationCommand;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<PasswordValidation, RegistrationCommand> {
 
    @Override
    public void initialize(PasswordValidation passwordValidation) {
    }
 
    @Override
    public boolean isValid(RegistrationCommand RegistrationCommand, ConstraintValidatorContext cxt) {
        if(!RegistrationCommand.getPassword().equals(RegistrationCommand.getPasswordConfirm())){
            cxt.disableDefaultConstraintViolation();
            cxt.buildConstraintViolationWithTemplate(cxt.getDefaultConstraintMessageTemplate())
                    .addPropertyNode("password")
                    .addConstraintViolation();
            cxt.buildConstraintViolationWithTemplate(cxt.getDefaultConstraintMessageTemplate())
                    .addPropertyNode("passwordConfirm")
                    .addConstraintViolation();
            return false;
        }
        return true;
    }
}