package nl.ecbonestroo.springthymeleafforms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringThymeleafFormsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringThymeleafFormsApplication.class, args);
	}

}
