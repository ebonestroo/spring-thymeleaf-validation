package nl.ecbonestroo.springthymeleafforms.dto;

import lombok.Data;
import nl.ecbonestroo.springthymeleafforms.validation.PasswordValidation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@PasswordValidation
public class RegistrationCommand {
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String password;
    @NotEmpty
    private String passwordConfirm;
}
